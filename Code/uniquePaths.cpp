class Solution
{
public:
    int uniquePaths(int m, int n)
    {
        int map[m][n];
        // Dynamic memory allocation
        // vector<int> row(n, 0);
        // vector<vector<int>> map(m, row);
        for(int i = 0; i < m; i++)
        {
            map[i][n - 1] = 1;
        }
        for(int i = 0; i < n; i++)
        {
            map[m - 1][i] = 1;
        }
        for(int x = m - 2; x >= 0; x--)
        {
            for(int y = n - 2; y >= 0; y--)
            {
                map[x][y] = map[x + 1][y] + map[x][y + 1];
            }
        }
        return map[0][0];
    }
};
