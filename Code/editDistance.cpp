// This code is ugly but it does work, oginaly i try to use shortest path but didn't not relize my mistake
// So this code is the result of that, here is a more elegant solution than my
// class Solution
// {
// public:
//     int minDistance(string s1, string s2)
//     {
//         vector<vector<int>> dp(s1.length()+1, vector<int> (s2.length()+1, 0));
//         for (int i = 0; i <= s2.length(); i++)
//             dp[0][i] = i;
//         for (int i = 0; i <= s1.length(); i++)
//             dp[i][0] = i;
//         for (int i = 1; i <= s1.length(); i++)
//         {
//             for (int j = 1; j <= s2.length(); j++)
//             {
//                 if (s1[i-1] == s2[j-1])
//                     dp[i][j] = dp[i-1][j-1];
//                 else
//                     dp[i][j] = 1 + min({dp[i-1][j-1], dp[i-1][j], dp[i][j-1]});
//             }
//         }
//         return dp[s1.length()][s2.length()];
//     }
// };
class Solution
{
public:
    int minDistance(string word1, string word2)
    {
        size_t len1 = word1.size();
        size_t len2 = word2.size();
        if(len1 == 0 && len2 == 0)
        {
            return 0;
        }
        else if(len1 == 0)
        {
            return len2;
        }
        else if(len2 == 0)
        {
            return len1;
        }
        struct node
        {
            bool vPenalty;
            bool hPenalty;
            int value;
        };
        vector<node> row(len1, node{false, false, 0});
        vector<vector<node>> dp(len2, row);
        if(word1[0] == word2[0])
        {
            dp[0][0].value = 0;
            dp[0][0].vPenalty = true;
            dp[0][0].hPenalty = true;
        }
        else
        {
            dp[0][0].value = 1;
        }
        for(size_t x = 1; x < len2; ++x)
        {
            if(dp[x - 1][0].hPenalty)
            {
                dp[x][0].value = dp[x - 1][0].value + 1;
                dp[x][0].hPenalty = dp[x - 1][0].hPenalty;
                dp[x][0].vPenalty = false;
            }
            else
            {
                if(word1[0] == word2[x])
                {
                    dp[x][0].value = dp[x - 1][0].value;
                    dp[x][0].hPenalty = true;
                    dp[x][0].vPenalty = false;
                }
                else
                {
                    dp[x][0].value = dp[x - 1][0].value + 1;
                    dp[x][0].hPenalty = false;
                    dp[x][0].vPenalty = false;
                }
            }
        }
        for(size_t y = 1; y < len1; ++y)
        {
            if(dp[0][y - 1].vPenalty)
            {
                dp[0][y].value = dp[0][y - 1].value + 1;
                dp[0][y].vPenalty = dp[0][y - 1].vPenalty;
                dp[0][y].hPenalty = false;
            }
            else
            {
                if(word1[y] == word2[0])
                {
                    dp[0][y].value = dp[0][y - 1].value;
                    dp[0][y].vPenalty = true;
                    dp[0][y].hPenalty = false;
                }
                else
                {
                    dp[0][y].value = dp[0][y - 1].value + 1;
                    dp[0][y].vPenalty = false;
                    dp[0][y].hPenalty = false;
                }
            }
        }
        
        for(size_t x = 1; x < len2; ++x)
        {
            for(size_t y = 1; y < len1; ++y)
            {
                node remove = dp[x][y - 1];
                node insert = dp[x - 1][y];
                node replace = dp[x - 1][y - 1];
                if(remove.value < insert.value && remove.value < replace.value)
                {
                    //remove smallest
                    if(remove.vPenalty)
                    {
                        dp[x][y].value = remove.value + 1;
                        dp[x][y].vPenalty = true;
                    }
                    else
                    {
                        if(word1[y] == word2[x])
                        {
                            dp[x][y].value = remove.value;
                            dp[x][y].vPenalty = true;
                        }else{
                            dp[x][y].value = remove.value + 1;
                        }
                    }
                }
                else if(insert.value < remove.value && insert.value < replace.value)
                {
                    //insert smallest
                    if(insert.hPenalty)
                    {
                        dp[x][y].value = insert.value + 1;
                        dp[x][y].hPenalty = true;
                    }
                    else
                    {
                        if(word1[y] == word2[x] || insert.hPenalty)
                        {
                            dp[x][y].value = insert.value;
                            dp[x][y].hPenalty = true;
                        }
                        else
                        {
                            dp[x][y].value = insert.value + 1;
                        }
                    }
                }
                else
                {
                    //replace smallest
                    if(word1[y] == word2[x])
                    {
                        dp[x][y].value = replace.value;
                        dp[x][y].hPenalty = true;
                        dp[x][y].vPenalty = true;
                    }
                    else
                    {
                        dp[x][y].value = replace.value + 1;
                    }
                }
            }
        }
        return dp[len2 - 1][len1 - 1].value;
    }
};
