class Solution
{
public:
    int climbStairs(int n)
    {
        int count[45];
        count[0] = 1;
        count[1] = 2;
        if(n == 1){
            return count[0];
        }
        if(n == 2)
        {
            return count[1];
        }
        for(int i = 0; i < n - 2; i++)
        {
            count[i+2] = count[i] + count[i+1];
        }
        return count[n-1];
    }
};

