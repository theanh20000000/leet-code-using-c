class Solution
{
public:
  int minPathSum(vector<vector<int>>& grid)
  {
    vector<vector<int>> newGrid = grid;
    newGrid[grid.size() - 1][grid[0].size() - 1] = grid[grid.size() - 1][grid[0].size() - 1];
    for(int y = grid[0].size() - 2; y >= 0; y--)
    {
        newGrid[grid.size() - 1][y] += newGrid[grid.size() - 1][y+1];
    }
    for(int x = grid.size() - 2; x >= 0; x--)
    {
        newGrid[x][grid[0].size() - 1] += newGrid[x + 1][grid[0].size() - 1];
    }
    for(int x = grid.size() - 2; x >= 0; x--)
    {
        for(int y = grid[0].size() - 2; y >= 0; y--)
        {
          newGrid[x][y] += min(newGrid[x + 1][y], newGrid[x][y + 1]);
        }
    }
    return newGrid[0][0];
  }
};
