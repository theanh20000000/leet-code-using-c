// Recursive way Not Accepted by LeetCode
// class Solution
// {
// public:
//     int lps(int l, int r, const string &s, map<pair<int, int>, int> &cache)
//     {
//         // cout<<"l: "<<l<<" r: "<<r<<"\n";
//         if(l < 0 || r > s.size() - 1)
//         {
//             return 0;
//         }
//         else if(s[l] == s[r])
//         {
//             // cout<<"match"<<endl;
//             pair<int, int> key{l ,r};
//             if(cache.contains(key))
//             {
//                 return 2 + cache[key];
//             }
//             else
//             {
//                 cache[key] = lps(l - 1, r + 1, s, cache);
//                 return 2 + cache[key];
//             }
//         }
//         else
//         {
//             return max(lps(l, r + 1, s, cache), lps(l - 1, r, s, cache));
//         }
//     }
//     int longestPalindromeSubseq(string s)
//     {
//         map<pair<int, int>, int> cache;
//         int max = 1;
//         for(int i = 0; i < s.size() - 1; i++)
//         {
//             int temp = 1 + lps(i - 1, i + 1, s, cache);
//             if(temp > max)
//             {
//                 max = temp;
//             }
//             // cout<<"result: "<<temp<<"\n";
//         }
//         for(int i = 0; i < s.size() - 1; i++)
//         {
//             int temp = lps(i, i + 1, s, cache);
//             if(temp > max)
//             {
//                 max = temp;
//             }
//             // cout<<"result: "<<temp<<"\n";
//         }
//         return max;
//     }
// };
class Solution
{
public:
    int longestPalindromeSubseq(string s)
    {
        size_t n = s.size();
        vector<size_t> row(n, 0);
        vector<vector<size_t>> dp(n, row);
        for(size_t x = 0; x < n; ++x)
        {
            dp[x][x] = 1;
        }
        for(size_t l = 1; l < n; ++l)
        {
            for(size_t x = 0; x < n - l; ++x)
            {
                size_t y = x + l;
                if(s[x] == s[y])
                {
                    dp[x][y] = 2 + dp[x + 1][y - 1];
                }
                else
                {
                    dp[x][y] = max(dp[x + 1][y], dp[x][y - 1]);
                }
            }
        }
        return dp[0][n - 1];
    }
};
