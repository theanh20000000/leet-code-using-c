class Solution
{
public:
    int minimumTotal(vector<vector<int>>& triangle)
    {
        int m = triangle.size();
        for(int x = m - 2; x >= 0; x--)
        {
            int n = triangle[x].size();
            for(int y = n - 1; y >= 0; y--)
            {
                triangle[x][y] += min(triangle[x + 1][y], triangle[x + 1][y + 1]);
            }
        }
        return triangle[0][0];
    }
};
