class Solution
{
public:
    int uniquePathsWithObstacles(vector<vector<int>>& obstacleGrid)
    {
        int m = obstacleGrid.size();
        int n = obstacleGrid[0].size();
        double map[m][n];

        if(obstacleGrid[m - 1][n - 1] == 1)
        {
            return 0;
        }
        else
        {
            map[m - 1][n - 1] = 1;
        }
        for(int i = m - 2; i >= 0; i--)
        {
            if(obstacleGrid[i][n - 1] == 1)
            {
                map[i][n - 1] = 0;
            }
            else
            {
                map[i][n - 1] = map[i + 1][n - 1];
            }
        }
        for(int i = n - 2; i >= 0; i--)
        {
            if(obstacleGrid[m - 1][i] == 1)
            {
                map[m - 1][i] = 0;
            }
            else
            {
                map[m - 1][i] = map[m - 1][i + 1];
            }
        }
        for(int x = m - 2; x >= 0; x--)
        {
            for(int y = n - 2; y >= 0; y--)
            {
                if(obstacleGrid[x][y] == 1)
                {
                    map[x][y] = 0;
                }
                else
                {
                    map[x][y] = map[x + 1][y] + map[x][y + 1];
                }
            }
        }
        return map[0][0];
    }
};
