class Solution
{
public:
    bool wordBreak(string s, vector<string>& wordDict)
    {
        vector<int> index = {0};
        vector<int> skipWord = {0};
        bool push = false;
        int layer = 0;

        //Sort wordDict for greedy
        for(int x = 0; x < wordDict.size(); x++)
        {
            for(int y = x; y< wordDict.size(); y++)
            {
                if(wordDict[y].size()> wordDict[x].size())
                {
                    swap(wordDict[y], wordDict[x]);
                }
            }
        }

        while(true)
        {
            push = false;
            if(skipWord[layer] >= wordDict.size())
            {
                break;
            }
            for(int i = 0 + skipWord[layer]; i < wordDict.size(); i++)
            {
                if(s.substr(index[layer], wordDict[i].size()) == wordDict[i])
                {
                    index.push_back(index[layer] + wordDict[i].size());
                    skipWord.push_back(0);
                    push = true;
                    skipWord[layer] = i;
                    layer++;
                    break;
                }
            }
            if(layer == 0 && push == false)
            {
                break;
            }
            if(push == false)
            {
                layer--;
                skipWord[layer]++;
                index.pop_back();
                skipWord.pop_back();
            }
            if(index[layer] == s.size())
            {
                return true;
            }
        }
        return false;
    }
};
