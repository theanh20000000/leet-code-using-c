class Solution
{
public:
    int min3(int a, int b, int c)
    {
        return min(a, min(b, c));
    }
    int minFallingPathSum(vector<vector<int>>& matrix)
    {
        int n = matrix.size();

        for(int x = n - 2; x >= 0; x--)
        {
            for(int y = 0; y < n; y++)
            {
                if(y == 0)
                {
                    matrix[x][0] += min(matrix[x + 1][0], matrix[x + 1][1]);
                }
                else if(y == n - 1)
                {
                    matrix[x][n - 1] += min(matrix[x + 1][n - 1], matrix[x + 1][n - 2]);
                }
                else
                {
                    matrix[x][y] += min3(matrix[x + 1][y - 1], matrix[x + 1][y], matrix[x + 1][y + 1]);
                }
            }
        }
        int minSum = matrix[0][n - 1];
        for(int i = n - 2; i >= 0; i--)
        {
            if(matrix[0][i] < minSum)
            {
                minSum = matrix[0][i];
            }
        }
        return minSum;
    }
};
