class Solution
{
public:
    int maximalSquare(vector<vector<char>>& matrix)
    {
        int m = matrix.size();
        int n = matrix[0].size();
        uint uintMatrix[m][n];
        uint result = 0;
        for(int x = m - 1; x >= 0; x--)
        {
            for(int y = n - 1; y >= 0; y--)
            {
                if(matrix[x][y] == '0')
                {
                    uintMatrix[x][y] = 0;
                }
                else
                {
                    uintMatrix[x][y] = 1;
                }
            }
        }
        for(int x = m - 2; x >= 0; x--)
        {
            for(int y = n - 2; y >= 0; y--)
            {
                if(uintMatrix[x][y] == 1)
                {
                    uint min = uintMatrix[x][y + 1];
                    if(uintMatrix[x + 1][y] < min)
                    {
                        min = uintMatrix[x + 1][y];
                    }
                    if(uintMatrix[x + 1][y + 1] < min)
                    {
                        min = uintMatrix[x + 1][y + 1];
                    }
                    uintMatrix[x][y] += min;
                }
            }
        }
        for(int x = 0; x < m; x++)
        {
            for(int y = 0; y < n; y++)
            {
                if(uintMatrix[x][y] > result)
                {
                    result = uintMatrix[x][y];
                }
            }
        }
        return result * result;
    }
};
