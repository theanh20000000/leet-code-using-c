class Solution
{
public:
    int deleteAndEarn(vector<int>& nums)
    {
        if(nums.size() == 1)
        {
            return nums[0];
        }
        else if((nums.size() == 2))
        {
            if(nums[0] != nums[1])
            {
                if(!(((nums[0]+1) == nums[1]) && ((nums[0]-1) == nums[1])))
                {
                    return nums[0] + nums[1];
                }
                else
                {
                    return max(nums[0], nums[1]);
                }
            }
            else
            {
                return nums[0] + nums[1];
            }
        }
        vector<int> newNums;
        vector<int> shadows;
        sort(nums.begin(), nums.end());
        int total = nums[0];
        for(int i = 1; i <= nums.size(); i++)
        {
            if(nums[i] == nums[i-1])
            {
                total += nums[i];
            }
            else
            {
                newNums.push_back(total);
                shadows.push_back(nums[i-1]);
                total = nums[i];
            }
        }
        if(newNums.size() == 1)
        {
            return newNums[0];
        }
        else if(newNums.size() == 2)
        {
            return max(newNums[0],newNums[1]);
        }
        int dp[shadows.size()];
        dp[shadows.size() - 1] = newNums[shadows.size() - 1];
        if(shadows[shadows.size() - 2] == shadows[shadows.size() - 1] - 1)
        {
            dp[shadows.size() - 2] = newNums[shadows.size() - 2];
        }
        else
        {
            dp[shadows.size() - 2] = newNums[shadows.size() - 2] + dp[shadows.size() - 1];
        }
        if(shadows[shadows.size() - 3] == shadows[shadows.size() - 2] - 1)
        {
            dp[shadows.size() - 3] = newNums[shadows.size() - 3] + dp[shadows.size() - 1];
        }
        else
        {
            dp[shadows.size() - 3] = newNums[shadows.size() - 3] + max(dp[shadows.size() - 1], dp[shadows.size() - 2]);
        }
        for(int i = shadows.size() - 4; i >= 0; i--)
        {
            if(shadows[i] == shadows[i+1]-1)
            {
                dp[i] = newNums[i] + max(dp[i + 2], dp[i + 3]);
            }
            else
            {
                dp[i] = newNums[i] + max(dp[i + 1], dp[i + 2]);
            }
        }
        return max(dp[0], dp[1]);

    }
};
