class Solution
{
public:
    string longestPalindrome(string s)
    {
        int len = s.length();
        int maxL = 0;
        int maxR = 0;
        for(int i = 0; i < len; i++)
        {
            int l = i;
            int r = i;
            while(l - 1 >= 0 && r + 1 <= len - 1 && s[l - 1] == s[r + 1])
            {
                l--;
                r++;
            }
            if(r - l > maxR - maxL)
            {
                maxR = r;
                maxL = l;
            }
        }
        for(int i = 0; i < len - 1; i++)
        {
            int l;
            int r;
            l = i;
            r = i + 1;
            if(s[r] == s[l]){
                while(l - 1 >= 0 && r + 1 <= len - 1 && s[l - 1] == s[r + 1])
                {
                    l--;
                    r++;
                }
                if(r - l > maxR - maxL)
                {
                    maxR = r;
                    maxL = l;
                }
            }
        }
        return s.substr(maxL, maxR - maxL + 1);
    }
};
