class Solution
{
public:
    int tribonacci(int n)
    {
        int tribonacci[n + 1];
        if(n == 0)
        {
            return 0;
        }
        else if(n == 1)
        {
            return 1;
        }
        else if(n == 2)
        {
            return 1;
        }
        else
        {
            tribonacci[0] = 0;
            tribonacci[1] = 1;
            tribonacci[2] = 1;
            for(int i = 3; i < n + 1; i++)
            {
                tribonacci[i] = tribonacci[i - 1] + tribonacci[i - 2] + tribonacci[i - 3];
            }
            return tribonacci[n];
        }
    }
};
